const animais = [
    {name: 'marcelo', especie: 'humano'},
    {name: 'sr. waddles', especie: 'hamster'},
    {name: 'toto', especie: 'cachorro'},
    {name: 'tom', especie: 'gato'},
    {name: 'jack', especie: 'cachorro'},
    {name: 'dom', especie: 'cachorro'}
];

let divisor = ({name, especie}) => {
    name = name || "nome não definido";
    especie = especie || "espécie não definida";
    console.log(`o nome é : ${name}, sua espécie é ${especie}`);
}

for(let i = 0; i< animais.length; i++){
    divisor(animais[i]);
}

let vetorNovo = animais.map((animal) => {
    return `${animal.name} é um ${animal.especie}`
});

/*
let cachorros = animais.filter((animal) => {
    return animal.especie === 'cachorro';
});

cachorros = [];
for (let i = 0; i< animais.length; i += 1){
    if(animais[i].especie === "cachorro"){
        cachorros.push(animais[i]);
    }
}


console.log(cachorros);
*/

console.log(vetorNovo);


const elementsList = document.querySelector("#elementos");
elementsList.addEventListener("click", (e) => {
    if(e.target.nodeName === 'LI') alert("clicou");
});
/*for(let i = 0; i<elementsList.childElementCount; i++){
    elementsList.children[i].addEventListener("click", (e) => alert("clicou"));
}

let newElement = document.createElement("li");
newElement.classList.add("list-item");
newElement.innerText = "CONTEUDO";
elementsList.appendChild(newElement);*/